import React, { Component } from 'react'
// import RobotStore from '../stores/RobotStore'
// import Robot from './Robot'

export default class RobotForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      type: '',
      mass: ''
    }
  }

  componentDidMount() {

  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  add = () => {
    this.props.onAdd({
      name: this.state.name,
      type: this.state.type,
      mass: this.state.mass
    })
  }

  render() {
    return (
      <div>
        <input type="text" placeholder="name" name="name" id="name" onChange={this.handleChange} />
        <input type="text" placeholder="type" name="type" id="type" onChange={this.handleChange} />
        <input type="text" placeholder="mass" name="mass" id="mass" onChange={this.handleChange} />
        <input type="button" value="add" onClick={this.add} />
      </div>
    )
  }
}